# CubSat project

Cougs in Space at Washington State University prototype data collection satellite project.

Designed to collection vibration data from a 1U cube satellite frame. Runs on a Raspberry Pi and uses the Pololu AltIMU-10 v5 inertial measurement unit for sensing.

This is a testing/prototype project as part of the CougSat I cubesat project.

Cougs in Space:
    http://cougsinspace.vcea.wsu.edu/


Contributors:
    Aaron S. Crandall <acrandal@gmail.com>
