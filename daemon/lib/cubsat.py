#!/usr/bin/env python
#
# Primary modules for the CubSat system operations
#
# Copyright - 2018
#
# Contributors:
#  Aaron S. Crandall <acrandal@gmail.com>
#

import RPi.GPIO as GPIO
from datetime import datetime
from time import time, sleep
from threading import *
from altimu.altimu import AltIMU, AltIMU_Slave, AltIMU_Master
from json import dumps


""" Wrapper class on LEDs to control lights
"""
class LED(object):
    def __init__(self, pin):
        self.pin = pin
        GPIO.setup(self.pin, GPIO.OUT)
        self.off()

    def on(self):
        GPIO.output(self.pin, GPIO.HIGH)
    def off(self):
        GPIO.output(self.pin, GPIO.LOW)


""" Root CubSat system object
"""
class CubSat(object):
    AMBER_LED_PIN = 27              # RPi BCM numbering for I/O pins
    GREEN_LED_PIN = 17

    def __init__(self, config):
        print("Creating CubSat system control")
        self.config = config            # Config from /etc/cubsat/cubsat.conf
        self.green_led = None           # LED object for green light
        self.amber_led = None           # LED object for amber light
        self.data_file_name = None      # FQN of data file
        self.data_filehandle = None     # Data file to write to
        self._is_setup = False          # Is hardware configured?
        self.done = False               # Should we stop collecting?
        self.is_running = False         # Are we collection now?
        self._my_thread = None          # Internal collection thread
        self.boardIMU = None            # Protoboard-mounted IMU
        self.frameIMU = None            # External satellite frame-mounted IMU

    """ Cleanly shutdown hardware and data file """
    def shutdown(self):
        if self._is_setup == True:
            print(" CubSat doing cleanup on hardware and files")
            self.data_filehandle.flush()
            self.data_filehandle.close()
            self.data_filehandle = None
            GPIO.cleanup()
            self._is_setup = False

    """ Destructor calls shutdown """
    def __del__(self):
        self.shutdown()

    """ Do all setup for CubSat systems """
    def setup(self):
        print("Setting up CubSat hardware and data logging files")
        self._setup_hardware()
        self._create_logfile()
        self._is_setup = True

    """ Make new logfile for our data store """
    def _create_logfile(self):
        file_name = datetime.now().strftime("%Y-%m-%d_%H:%M:%S.dat")
        data_dir = self.config["datadir"]
        self.data_file_name = data_dir + "/" + file_name
        print(" Creating logfile: " + self.data_file_name)
        self.data_filehandle = open( self.data_file_name, 'w' )
        print("  -- Created ")

    """ Setup the two Pololu AltIMU-10 v5's """
    def _setup_IMUs(self):
        print("  Connecting to AltIMU devices ")
        self.boardIMU = AltIMU_Slave()          # SA0 pin grounded
        self.frameIMU = AltIMU_Master()         # SA0 pin floating (default)

        self.boardIMU.enable_accelerometer()
        self.frameIMU.enable_accelerometer()

        self.boardIMU.setAccelerometerFullScale16G()
        self.frameIMU.setAccelerometerFullScale16G()

        self.boardIMU.enable_gyroscope()
        self.frameIMU.enable_gyroscope()
        # Do we need to change the scale on the gyroscopes?
        # Do we need the magnetometers?

        self.frameIMU.enable_thermometer()
        self.frameIMU.enable_barometer()
        print("     -- Devices enabled")


    """ Initialize GPIO and sensor sources """
    def _setup_hardware(self):
        print(" Setting up CubSat hardware - GPIO and I2C devices")
        GPIO.setmode(GPIO.BCM)

        self.green_led = LED(CubSat.GREEN_LED_PIN)
        self.amber_led = LED(CubSat.AMBER_LED_PIN)

        self._setup_IMUs()
        self.amber_led.on()                     # User feedback is good

    
    """ Stop data collection thread (if running) """
    def stop(self):
        print(" CubSat system being ordered to stop")
        self.done = True

    """ Start thread for data collection """
    def run(self):
        print(" Starting CubSat data collection thread")
        if not self._is_setup:
            print("  -- FAIL: CubSat not setup - call setup() first.")
            return
        self._my_thread = Thread(target=self.run_collection)
        self._my_thread.start()
        self.is_running = True

    """ Caller will join (wait) on the CubSat collection thread """
    def join(self):
        if self.is_running:
            print(" Sleeping on CubSat collection thread")
            self._my_thread.join()

    """ Collection function (used for thread) """
    def run_collection(self):
        print(" Starting data collection")
        self.green_led.on()                 # User feedback is good
        target_hz = 40
        collection_start_time = time()
        sample_count = 0
        timing_error_count = 0
        sample_delayed = False
        while not self.done:
            start_time = time()
            self._do_sample(delayed=sample_delayed)
            end_time = time()
            pause_secs = 1.0/target_hz - (end_time - start_time) # target 40 Hz
            print(" run secs: " + str(time() - collection_start_time) + " -- slp: " + str(pause_secs) + " -- Err: " + str(timing_error_count) + " / " + str(sample_count))
            sample_count += 1
            if pause_secs > 0:
                sample_delayed = False
                sleep(pause_secs)
            else:
                sample_delayed = True
                timing_error_count += 1
                #print(" ** WARNING: Sampling taking more than 0.025 secs ** ")
        print(" Collection stopped by done flag")
        self.green_led.off()
        self.is_running = False         # Know our thread is done
        self.done = False               # Reset done flag for next run() call
        self._my_thread = None          # Lose the old thread

    """ Take a sample from all sensors and write to SD card data file """
    def _do_sample(self, delayed):
        start_time          = time()
        boardAcceleration   = self.boardIMU.getAcceleration()
        boardGyroscope      = self.boardIMU.getGryoscopeDPS()

        frameAcceleration   = self.frameIMU.getAcceleration()
        frameGyroscope      = self.frameIMU.getGryoscopeDPS()

        altitude            = self.frameIMU.getAltitude()
        temperature         = self.frameIMU.getTemperatureCelsius()

        data = {}
        data["epoch"] = start_time
        data["delayed"] = delayed
        data["boardAcceleration"] = { "x" : boardAcceleration[0],
                                      "y" : boardAcceleration[1],
                                      "z" : boardAcceleration[2] }
        data["boardGyroscope"] = { "x" : boardGyroscope[0],
                                   "y" : boardGyroscope[1],
                                   "z" : boardGyroscope[2] }
        data["frameAcceleration"] = { "x" : frameAcceleration[0],
                                      "y" : frameAcceleration[1],
                                      "z" : frameAcceleration[2] }
        data["frameGyroscope"] = { "x" : frameGyroscope[0],
                                   "y" : frameGyroscope[1],
                                   "z" : frameGyroscope[2] }
        data["altitude"] = altitude
        data["temperature"] = temperature

        self.data_filehandle.write(dumps(data))
        self.data_filehandle.write('\n')
        self.data_filehandle.flush()
