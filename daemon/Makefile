#
# Makefile to install and enable the CubSat system control daemon
#
# Copyright 2018
#  Aaron S. Crandall <acrandal@gmail.com>
#
#

CUBSATINITSCRIPT=cubsat
CUBSATSRC=/usr/local/cubsat
CUBSATUSER=astro
CUBSATCONFIGDIR=/etc/cubsat

all:
	@echo "Infomative message: "make install" to install, "make uninstall" to uninstall"
	@echo " make: install | uninstall | start | stop"

install: cubsat.init cubsatd cubsat.conf
	@echo "Installing CubSat system daemon / service"
	adduser --system $(CUBSATUSER) --group --no-create-home
	usermod -a -G gpio,i2c $(CUBSATUSER)
	# Setup init scripts and init system
	cp ./cubsat.init /etc/init.d/cubsat
	chmod +x /etc/init.d/cubsat
	# Setup /usr/local binaries and libraries
	mkdir -p $(CUBSATSRC)/bin
	mkdir -p $(CUBSATSRC)/lib
	cp ./cubsatd $(CUBSATSRC)/bin/
	cp -r ./lib/* $(CUBSATSRC)/lib/
	chown -R $(CUBSATUSER):$(CUBSATUSER) $(CUBSATSRC)
	chmod +x $(CUBSATSRC)/bin/cubsatd
	# setup /var data store
	mkdir -p /var/$(CUBSATINITSCRIPT)/data
	mkdir -p /var/$(CUBSATINITSCRIPT)/log
	chown -R $(CUBSATUSER):$(CUBSATUSER) /var/$(CUBSATINITSCRIPT)
	# Setup /etc/ configuration location
	mkdir -p $(CUBSATCONFIGDIR)
	cp ./cubsat.conf $(CUBSATCONFIGDIR)/
	chown -R $(CUBSATUSER):$(CUBSATUSER) $(CUBSATCONFIGDIR)
	# Reload the init script configurations
	update-rc.d $(CUBSATINITSCRIPT) defaults

uninstall:
	service $(CUBSATINITSCRIPT) stop || true
	rm /etc/init.d/cubsat || true
	rm -r $(CUBSATSRC) || true
	rm -r /var/$(CUBSATINITSCRIPT) || true
	rm -r $(CUBSATCONFIGDIR) || true
	update-rc.d $(CUBSATINITSCRIPT) defaults
	deluser --system $(CUBSATUSER)

start:
	service $(CUBSATINITSCRIPT) start || echo "run make install first?"

stop:
	service $(CUBSATINITSCRIPT) stop || true

restart:
	service $(CUBSATINITSCRIPT) restart || true

test:
	sudo -u astro /usr/local/cubsat/bin/cubsatd
