

import RPi.GPIO as GPIO
import time

def my_callback(channel):
    print('This is a edge event callback function!')
    print('Edge detected on channel %s'%channel)
    print('This is run in a different thread to your main program')

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)

GPIO.add_event_detect(18, GPIO.BOTH, callback=my_callback, bouncetime=500)

GPIO.output(17, GPIO.HIGH)
GPIO.output(27, GPIO.HIGH)

for i in range(5):
    time.sleep(1)
    print("loop")

GPIO.output(17, GPIO.LOW)
GPIO.output(27, GPIO.LOW)

GPIO.cleanup()
